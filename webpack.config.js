const HtmlWebPackPlugin = require("html-webpack-plugin");

const path = require("path");

module.exports = {
    entry: {index: path.resolve(__dirname, "src", "index.js")},
    output: {
        path: path.resolve(__dirname, "build", "main.js")
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },
    optimization: {
        splitChunks: { chunks: "all" }
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: path.resolve(__dirname, "src", "index.html")
        })
    ]
}